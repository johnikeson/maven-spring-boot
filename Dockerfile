FROM openjdk:23-ea-jdk-slim-bullseye

WORKDIR /usr/app

COPY ./target/react-and-spring-data-rest-0.0.1-SNAPSHOT.jar /usr/app/

ENTRYPOINT ["java", "-jar", "react-and-spring-data-rest-0.0.1-SNAPSHOT.jar"]

EXPOSE 8081
